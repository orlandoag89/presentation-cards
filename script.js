class PresentationCard extends HTMLElement {
	
  constructor() {
	super();
	this.shadow = this.attachShadow({mode : 'open'});
	this.elemento = document.createElement('div');
	this.elemento.textContent = 'hola';
	this.shadow.appendChild(this.elemento);
  }
  
  connectedCallback() {
  }
  
  disconnectedCallback() {
  }
  
  attributeChangedCallback(name, oldValue, newValue) {
	this.getAtributos();
	this.elemento.textContent = this.getSaludo();
  }
  
  static get observedAttributes() {
	return ['name', 'last-name', 'email'];
  }
 
  getSaludo() {
	let saludo = '';
	let soyYo = this.name.toLowerCase() === 'orlando' && this.lastName.toLowerCase() === 'angeles';
	if (soyYo) {
	  saludo = 'Yo soy ';
	} else {
	  saludo = 'Tu nombre es ';
	}
	saludo = saludo + `${this.name} ${this.lastName}, ${this.getSerParteDeSkynet(soyYo)}`;
	return saludo;
  }	  
  
  getSerParteDeSkynet(soyYo) {
	let isSkynet = this.email.includes('gmail.com');
	let output = '';
	if (soyYo && isSkynet) {
	  output = 'soy parte de skynet';
	} else {
	  if (soyYo && !isSkynet) {
		output = 'no soy parte de skynet';
	  } else {
	    if (isSkynet) {
		  output = 'eres parte de skynet';
	    } else {
		  output = 'no eres parte de skynet';
	    }
	  }
	}
	return output;
  }
  
  getAtributos() {
	this.name = this.getAttribute('name');
	this.lastName = this.getAttribute('last-name');
	this.email = this.getAttribute('email');
  }
  
}

customElements.define('presentation-card-ooas', PresentationCard);